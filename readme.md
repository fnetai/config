# @fnet/config

The `@fnet/config` module is a utility designed to simplify the process of managing and accessing configuration files for applications. By focusing on ease of use and flexibility, it helps users retrieve configuration settings from YAML files located in designated directories. It caters to both single and batch processing of configuration files and allows users to work with custom environmental variables seamlessly.

## How It Works

The module scans specified directories to locate YAML configuration files that match the provided name(s). Users can specify a default directory, relative paths, or even use the current working directory. Once a configuration is found, the module parses it and optionally updates the system's environmental variables based on the file's content.

## Key Features

- **Directory Searching**: Automatically searches in a sequence of specified directories and relative paths for configuration files.
- **YAML Parsing**: Processes YAML files and extracts their data for easy access.
- **Environmental Variable Integration**: Optionally injects configuration settings into the system’s environment variables.
- **Batch Processing**: Supports handling multiple configuration files at once by accepting arrays of names.
- **Tag Filtering**: Allows selective processing of YAML content based on specified tags.
- **Optional File Presence**: Users can specify whether the absence of a configuration file should trigger an error.

## Conclusion

The `@fnet/config` module provides a straightforward way to manage configuration files, making it easier for users to organize and access application settings. With its uncomplicated setup and useful features, it offers a practical solution for managing application configurations without unnecessary complexity.