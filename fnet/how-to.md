# @fnet/config Developer Guide

## Overview

The `@fnet/config` library simplifies the process of loading and managing configuration files in your applications. It is particularly useful for environments where configurations need to be dynamic and flexible. The library allows developers to effortlessly locate configuration files across specified directories and optionally transfer configuration-defined environment variables to `process.env`.

## Installation

To use `@fnet/config` in your project, you can install it via npm or yarn with the following commands:

Using npm:
```bash
npm install @fnet/config
```

Using yarn:
```bash
yarn add @fnet/config
```

## Usage

The library provides a default asynchronous function designed to locate and process configuration files, handling various extensions such as `.fnet`, `.fnet.yaml`, and `.fnet.yml`. Below is a step-by-step guide on how to use the library.

```javascript
import loadConfig from '@fnet/config';

async function loadMyConfig() {
  try {
    const config = await loadConfig({
      name: 'myConfig', // Name of the config file without extension
      dir: ['path/to/config', 'another/path'], // Directories to search
      rel: ['./.fnet/'], // Relative paths within directories
      transferEnv: true, // Transfer env variables to process.env
      optional: false, // Throws error if the file is not found
      tags: ['production'] // Filter by specific tags in the YAML file
    });

    console.log('Loaded configuration:', config);
  } catch (error) {
    console.error('Error loading configuration:', error);
  }
}

loadMyConfig();
```

## Examples

Below are examples demonstrating typical usage scenarios for `@fnet/config`.

### Example 1: Loading a Single Configuration File

```javascript
import loadConfig from '@fnet/config';

async function loadSingleConfig() {
  const config = await loadConfig({ name: 'app-config' });
  console.log(config.data); // Outputs the parsed configuration data
}
```

### Example 2: Loading Multiple Configuration Files

```javascript
import loadConfig from '@fnet/config';

async function loadMultipleConfigs() {
  const configs = await loadConfig({
    name: ['db-config', 'api-config'], // Multiple config names
    dir: process.cwd() // Current working directory
  });
  
  configs.forEach(config => console.log(config.file, config.data));
}
```

### Example 3: Handling Optional Configurations

```javascript
import loadConfig from '@fnet/config';

async function loadOptionalConfig() {
  try {
    const config = await loadConfig({
      name: 'optional-config',
      optional: true // No error if file is not found
    });
    
    console.log('Optional configuration loaded:', config);
  } catch (e) {
    console.log('Config file is optional, proceeding without:', e.message);
  }
}
```

## Acknowledgement

While the `@fnet/config` library does not require explicit acknowledgment of external libraries for basic usage, it leverages `@fnet/yaml` for parsing YAML files, ensuring efficient and reliable handling of configuration files.