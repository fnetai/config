import fs from "node:fs";
import path from "node:path";
import parseYaml from "@fnet/yaml";

/**
 * Processes the input 'name' by searching for a related configuration file in specified directories.
 * It supports batch processing if 'name' is an array.
 * The function normalizes the paths to ensure compatibility across different operating systems, including Windows.
 * 
 * @param {Object} args - The arguments object.
 * @param {string|string[]} args.name - The name of the configuration or an array of names.
 * @param {string|string[]} [args.dir=process.cwd()] - The directories to search within.
 * @param {string[]} [args.rel=['./.fnet/', '../.fnet/', '../../.fnet/', '../../../.fnet/']] - Relative paths to consider within directories, will be normalized.
 * @param {boolean} [args.transferEnv=true] - Whether to transfer 'env' values from the configuration to process.env.
 * @param {boolean} [args.optional=false] - Whether the presence of the file is optional.
 * @param {boolean} [args.tags=[]] - An array of tags used for filtering YAML content. Only content marked with tags present in this array will be processed. 
 * @returns {Promise<Object|Object[]>} The file path and parsed data, or an array of such objects if 'name' is an array.
 */
export default async ({
  name,
  dir = process.cwd(),
  rel = ['./.fnet/', '../.fnet/', '../../.fnet/', '../../../.fnet/'],
  transferEnv = true,
  optional = false,
  tags = []
} = {}) => {
  name = name || '';
  return Array.isArray(name)
    ? await Promise.all(name.map(singleName => processConfig(singleName, dir, rel, transferEnv, optional, tags)))
    : await processConfig(name, dir, rel, transferEnv, optional, tags);
};

async function processConfig(name, dir, rel, transferEnv, optional, tags) {
  const dirs = [].concat(dir, process.env.HOME || process.env.USERPROFILE).map(d => path.normalize(d));
  const rels = [].concat(rel).map(r => path.normalize(r));
  const extensions = ['.fnet', '.fnet.yaml', '.fnet.yml'];

  for (const currentDir of dirs) {
    for (const currentRel of rels) {
      for (const extension of extensions) {
        const filePath = path.resolve(currentDir, currentRel, `${name}${extension}`);
        if (fs.existsSync(filePath)) {
          const { raw, parsed } = await parseYaml({ file: filePath, tags });
          if (transferEnv && parsed.env && typeof parsed.env === "object") {
            Object.assign(process.env, parsed.env);
          }

          // TODO: remove duplicate!!!
          return {
            file: filePath,
            data: parsed,
            config: parsed,
            raw: raw
          };
        }
      }
    }
  }

  if (!optional) {
    throw new Error(`Unable to find '${name}' in any of the following directories: ${dirs.join(", ")}`);
  }
};