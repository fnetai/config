import Node from "../src";

export default async (args) => {
  const result = await Node(args);
  console.log(result);
  return result;
}